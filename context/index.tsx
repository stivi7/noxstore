import React from 'react';

export const NoxCtx: any = React.createContext(null);

export const NoxProvider = ({ store, children }: { store: any, children: any }) =>
  <NoxCtx.Provider value={store}>{ children }</NoxCtx.Provider>