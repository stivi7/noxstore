import { useContext, useEffect, useState } from 'react';
import { Observable } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';

import { IState } from './../interfaces/index';
import { NoxCtx } from './../context/index';

export const useNoxSelector = (cb: (state: IState) => any): any => {
  const store: any = useContext(NoxCtx);
  const state$: Observable<IState> = store.getState$();

  const desiredChunk$: Observable<any> = state$.pipe(
    map(cb),
    distinctUntilChanged(),
  )

  const [chunk, setChunk] = useState(undefined);

  useEffect(() => {
    const sub = desiredChunk$.subscribe(
      (chunk) => setChunk(chunk)
    )

    return () => sub.unsubscribe()
  }, []);

  return chunk;
}

export const useNoxDispatcher = () => {
  const store: any = useContext(NoxCtx);

  return store.dispatch;
}