import { IConfigureStore } from './interfaces/index';
import { Store } from './store';
import { Dispatcher } from './dispatcher';
import { NoxCtx, NoxProvider } from './context';
import { useNoxSelector, useNoxDispatcher } from './hooks';
import { 
  IConfigureStoreModels,
  IConfigureStore,
  IDispatcherRegistry,
  IStoreState,
} from './interfaces';

const configureStore = (models: IConfigureStoreModels): IConfigureStore => {
  const dispatcherModels: IDispatcherRegistry = {};
  const stateModels: IStoreState = {};

  const modelsKeys = Object.keys(models);

  if (!modelsKeys.length) {
    throw new Error('No models provided');
  }

  modelsKeys.forEach((modelKey) => {
    // Fills the dispatcher and store registries
  
    const model = models[modelKey];
    dispatcherModels[modelKey] = {};
    stateModels[modelKey] = {};

    const modelItemsKeys = Object.keys(model);

    modelItemsKeys.forEach((itemKey) => {
      if (typeof model[itemKey] === 'function') {
        dispatcherModels[modelKey][itemKey] = model[itemKey];
      } else {
        stateModels[modelKey][itemKey] = model[itemKey];
      }
    })
  })

  const store = new Store(stateModels);
  const dispatcher = new Dispatcher(dispatcherModels);
  
  const bundledStore: IConfigureStore = {
    ...store,
    ...dispatcher,
  }
  return bundledStore;
}

export default {
  configureStore,
  NoxCtx,
  NoxProvider,
  useNoxSelector,
  useNoxDispatcher,
};