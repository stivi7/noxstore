import { BehaviorSubject, Observable } from 'rxjs';
import { IState, IDispatchOptions, IStore } from '../interfaces'

// save state obs$ as a property of class
// configure multicast
export class Store {
  private subject: BehaviorSubject<object>;

  constructor(initialState: IState) {
    this.subject = new BehaviorSubject(initialState);
    this.dispatch = this.dispatch.bind(this);
    this.getState$ = this.getState$.bind(this);
  }

  public getState$(): Observable<IState> {
    return this.subject.asObservable();
  }

  // Add middleware support
  
  public dispatch(options: IDispatchOptions): void {
    const { path, setPath } = options;

    const state: IState = this.subject.getValue();
    const newState: object = {
      ...state,
      [path]: setPath(state[path]),
    }

    this.subject.next(newState);
  }
}