import { BehaviorSubject, Observable } from 'rxjs';

// To be removed
export interface IDispatchOptions {
  path: string;
  setPath: Function;
};

/**
 * Dispatcher interfaces
 */

export interface IDispatcher {

}

export interface IDispatcherRegistry {
  [key: string]: {
    [key: string]: TMethodToSetState;
  }
}

export interface IAction {
  stateModel: string;
  stateMethod: TMethodToSetState
}

export type TMethodToSetState = (state: IState, payload: any) => any;

/**
 * Store interfaces
 */

export interface IState {
  [key: string]: any
};



export interface IStoreState {
  [key: string]: IState;
}

export interface IStore {
  subject: BehaviorSubject<IState>,
  getState$: () => Observable<IState>,
  dispatch: (options: IDispatchOptions) => void,
};

export interface IStoreModel {
  [key: string]: any;
};

export interface IConfigureStoreModels {
  [key: string]: IStoreModel;
};

export interface IConfigureStore extends IDispatcher, IStore {}