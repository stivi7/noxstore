import React from 'react';
import logo from './logo.svg';
import './App.css';

import noxBag from './nox_dist';

const { useNoxSelector, useNoxDispatcher } = noxBag;

const Counter = () => {
  const counter = useNoxSelector((state) => state.counter);
  const dispatcher = useNoxDispatcher();

  console.log(dispatcher);
  const setCounter = () => {
    dispatcher({
      path: 'counter',
      setPath: (counter) => counter + 1,
    })
  }
  return (
    <div>
      <h1>Counter: {counter}</h1>
      <button onClick={setCounter}>Count</button>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
