export interface IDispatchOptions {
    path: string;
    setPath: Function;
}
export interface IState {
    [key: string]: any;
}
