import { Observable } from 'rxjs';
import { IState, IDispatchOptions } from '../interfaces';
export declare class Store {
    private subject;
    constructor(initialState: IState);
    getState$(): Observable<IState>;
    dispatch(options: IDispatchOptions): void;
}
