"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
// save state obs$ as a property of class
// configure multicast
var Store = /** @class */ (function () {
    function Store(initialState) {
        this.subject = new rxjs_1.BehaviorSubject(initialState);
        this.dispatch = this.dispatch.bind(this);
        this.getState$ = this.getState$.bind(this);
    }
    Store.prototype.getState$ = function () {
        return this.subject.asObservable();
    };
    Store.prototype.dispatch = function (options) {
        var _a;
        var path = options.path, setPath = options.setPath;
        var state = this.subject.getValue();
        var newState = __assign({}, state, (_a = {}, _a[path] = setPath(state[path]), _a));
        this.subject.next(newState);
    };
    return Store;
}());
exports.Store = Store;
