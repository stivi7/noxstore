"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var operators_1 = require("rxjs/operators");
var index_1 = require("./../context/index");
exports.useNoxSelector = function (cb) {
    var store = react_1.useContext(index_1.NoxCtx);
    var state$ = store.getState$();
    var desiredChunk$ = state$.pipe(operators_1.map(cb));
    var _a = react_1.useState(undefined), chunk = _a[0], setChunk = _a[1];
    react_1.useEffect(function () {
        var sub = desiredChunk$.subscribe(function (chunk) { return setChunk(chunk); });
        return function () { return sub.unsubscribe(); };
    }, []);
    return chunk;
};
exports.useNoxDispatcher = function () {
    var store = react_1.useContext(index_1.NoxCtx);
    return store.dispatch;
};
