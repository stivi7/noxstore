import { IState } from './../interfaces/index';
export declare const useNoxSelector: (cb: (state: IState) => any) => any;
export declare const useNoxDispatcher: () => any;
