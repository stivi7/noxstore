/// <reference types="react" />
import { Store } from './store';
declare const _default: {
    NoxStore: typeof Store;
    NoxCtx: any;
    NoxProvider: ({ store, children }: {
        store: any;
        children: any;
    }) => JSX.Element;
    useNoxSelector: (cb: (state: import("../../../../Users/softup/Desktop/NoxStore/interfaces").IState) => any) => any;
    useNoxDispatcher: () => any;
};
export default _default;
