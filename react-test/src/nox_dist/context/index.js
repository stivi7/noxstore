"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
exports.NoxCtx = react_1.default.createContext(null);
exports.NoxProvider = function (_a) {
    var store = _a.store, children = _a.children;
    return react_1.default.createElement(exports.NoxCtx.Provider, { value: store }, children);
};
