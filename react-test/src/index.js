import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import noxBag from './nox_dist';

const { NoxStore, NoxProvider } = noxBag;

const store = new NoxStore({
  counter: 0,
  secondCounter: 1,
})
console.log(store)

ReactDOM.render(<NoxProvider store={store}><App /></NoxProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
