import { IDispatcherRegistry } from '../interfaces';

export class Dispatcher {
  constructor(dispatcherModels: IDispatcherRegistry) {
    console.log(dispatcherModels);
  }
  // Create a registry that holds state change callbacks
  // These callbacks will be turned in methods which return a plain object 
  // with info needed for the store to change the state

  // { storePath, method }
};
